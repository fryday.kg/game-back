from flask import Flask, request, render_template
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from entities import Base
from entities.Achievment import get_achievments, set_ach
from entities.Score import get_top, set_score
from entities.Star import set_star, get_star

app = Flask('Scores')
app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True

engine = create_engine('sqlite:///database.tmp', echo=True)
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()


@app.route('/scores')
def top():
    scores = get_top(session, 20)
    for score in scores:
      score.ach = get_achievments(session, score.username).achievments
    return render_template('scores.html', scores=scores)

@app.route('/score', methods=['GET', 'POST'])
def set_score_get_post():
    print('Headers: ', request.headers)
    if request.method == 'GET':
        score  = int(request.args.get('score', '0'))
        return render_template('set_score.html', score=score)
    else:
        username = request.form['username']
        score = request.form['score']
        if not username:
            return render_template('set_score.html', score=score)
        if username and score:
            set_score(session, username.strip(), int(score))
            if username.strip() == get_top(session, 1)[0].username:
              set_ach(session, username.strip(), 'top')
            else:
              set_ach(session, username.strip(), '')
            star = get_star(session, username.strip())
            if star:
                return 'OK', 201
            else:
                return render_template('set_star.html', username=username.strip())


@app.route('/star', methods=['POST'])
def set_star_post():
    username = request.form['username']
    rate = request.form['rate']
    if username and rate:
        set_star(session, username.strip(), int(rate))
    return 'OK', 201

@app.route('/about', methods=['GET'])
def about():
    return render_template('about.html')

app.run(host='localhost',port=1234)
