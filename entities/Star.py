from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean

from entities import Base


class Star(Base):
  __tablename__ = 'stars'

  username = Column(String, primary_key=True)
  rate = Column(Integer, primary_key=True)


def set_star(session, username, rate):
  star = session.query(Star).filter_by(username = username).first()
  if star:
    star.rate = rate
  else:
    star = Star(username = username, rate = rate)
    session.add(star)
  session.commit()
  return star

def get_star(session, username):
  star = session.query(Star).filter_by(username=username).first()
  return star