from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

import entities.Score
import entities.Star
import entities.Achievment