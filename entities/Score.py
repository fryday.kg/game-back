from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean

from entities import Base


class Score(Base):
  __tablename__ = 'score'

  username = Column(String, primary_key=True)
  score = Column(Integer, primary_key=True)


def set_score(session, username, new_score):
  score = session.query(Score).filter_by(username = username).first()
  if score:
    score.score = max(new_score, score.score)
  else:
    score = Score(username = username, score = new_score)
    session.add(score)
  session.commit()
  return score


def get_top(session, top = 10):
  scores = session.query(Score).order_by(Score.score.desc()).limit(top).all()
  return scores