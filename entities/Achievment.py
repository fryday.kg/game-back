from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean

from entities import Base


class Achievment(Base):
  __tablename__ = 'achievment'

  username = Column(String, primary_key=True)
  achievments = Column(String, primary_key=True)


def set_ach(session, username, new_ach):
  score = session.query(Achievment).filter_by(username = username).first()
  if score:
    achs = set(score.achievments.split(','))
    achs.add(new_ach)
    score.achievments = ','.join(achs)
  else:
    score = Achievment(username = username, achievments = new_ach)
    session.add(score)
  session.commit()
  return score

def get_achievments(session, username):
  star = session.query(Achievment).filter_by(username=username).first()
  return star
